﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlPlayer : MonoBehaviour
{
    public Text scoreText;
    private int score;
    private Rigidbody rigidB;
    private readonly float speed = 2f;

    void Start()
    {
        rigidB = gameObject.GetComponent<Rigidbody>();
        score = 0;
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("point"))
        {
            score++;
            scoreText.text = "Score:"+score;
            Destroy(collision.gameObject);
        }
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.UpArrow))
        {
            rigidB.AddForce(Vector3.forward * speed);
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            rigidB.AddForce(Vector3.back * speed);
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            rigidB.AddForce(Vector3.right * speed);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rigidB.AddForce(Vector3.left * speed);
        }
    }
}
