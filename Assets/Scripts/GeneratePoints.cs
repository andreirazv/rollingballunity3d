﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratePoints : MonoBehaviour
{
    public GameObject PointPrefab;
    private int pointsSize = 20;
    void Start()
    {
        for(int i = 0; i < pointsSize; i++)
        {
            GameObject newGO = Instantiate(PointPrefab);
            newGO.transform.position = new Vector3(Random.Range(-23, 23), 0.4f, Random.Range(-23, 23));
            newGO.transform.SetParent(transform);
        }
    }
}
