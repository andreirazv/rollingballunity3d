﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class HandleAction : MonoBehaviour
{
    public void OnPressStart()
    {
        SceneManager.LoadScene(1);
    } 
    public void OnPressMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void OnPressExitGame()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }
}
